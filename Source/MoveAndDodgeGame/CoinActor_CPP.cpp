// Fill out your copyright notice in the Description page of Project Settings.


#include "CoinActor_CPP.h"

#include "Player_CPP.h"
#include "Kismet/GameplayStatics.h"
#include "Runtime/UMG/Public/Blueprint/UserWidget.h"

// Sets default values
ACoinActor_CPP::ACoinActor_CPP()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ACoinActor_CPP::BeginPlay()
{
	Super::BeginPlay();	
}

// Called every frame
float r = 0;
void ACoinActor_CPP::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	r += 1.0f * DeltaTime;
	SetActorRelativeRotation(FRotator(0.0f, r, 0.0f), false, nullptr);
}

void ACoinActor_CPP::NotifyActorBeginOverlap(AActor* OtherActor)
{
	Super::NotifyActorBeginOverlap(OtherActor);
	if(OtherActor->ActorHasTag(FName(TEXT("Player"))) == true)
	{
		GLog->Log("Collected");
		Cast<APlayer_CPP>(OtherActor)->SetScore(100);
		Cast<APlayer_CPP>(OtherActor)->UpdateScore();	
		UGameplayStatics::SpawnSound2D(this, PickupCoinSnd);
		GetWorld()->SpawnActor<AActor>(ParticleBurstObj, GetActorLocation(), GetActorRotation());
		Destroy();
	}
}
