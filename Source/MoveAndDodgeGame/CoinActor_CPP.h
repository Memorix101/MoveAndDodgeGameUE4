// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CoinActor_CPP.generated.h"

UCLASS()
class MOVEANDDODGEGAME_API ACoinActor_CPP : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACoinActor_CPP();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	UPROPERTY(EditAnywhere, Category = CPP)
	USoundBase* PickupCoinSnd;
	UPROPERTY(EditAnywhere, Category = CPP)
	TSubclassOf<AActor> ParticleBurstObj;
	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;
};
