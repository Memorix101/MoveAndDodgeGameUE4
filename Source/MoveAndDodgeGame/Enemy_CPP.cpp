// Fill out your copyright notice in the Description page of Project Settings.


#include "Enemy_CPP.h"
#include "Components/StaticMeshComponent.h"
#include "DrawDebugHelpers.h"

// Sets default values
AEnemy_CPP::AEnemy_CPP()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	GLog->Log("AEnemy_CPP()");

	RootComponentMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Root Component Mesh"));
}

void AEnemy_CPP::CollisionRight(UStaticMeshComponent* me)
{
	FVector actor_pos = GetActorLocation();
	FVector actor_vec_r = GetActorRightVector() * 100.0f;
	FHitResult OutHit;
	FCollisionQueryParams CollisionParams;
	CollisionParams.bTraceComplex = false;
	CollisionParams.AddIgnoredActor(this);

	//DrawDebugLine(GetWorld(), actor_pos, actor_pos + actor_vec_r, FColor::Red, false);
	//GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, actor_pos.ToString());
	
	if (GetWorld()->LineTraceSingleByChannel(OutHit, actor_pos, actor_pos + actor_vec_r, ECollisionChannel::ECC_Visibility, CollisionParams))
	{
		AActor* OtherActor  = OutHit.GetActor(); 
		if (OtherActor->ActorHasTag(FName(TEXT("Wall"))) == true)
		{
			//GLog->Log(OtherActor->Tags.Last().ToString());
			move_right = false;
		}

		if (OutHit.bBlockingHit)
		{
			//GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red,FString::Printf(TEXT("You are hitting: %s"), *OutHit.GetActor()->GetName()));
			//GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, FString::Printf(TEXT("Impact Point: %s"), *OutHit.ImpactPoint.ToString()));
			//GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, FString::Printf(TEXT("Normal Point: %s"), *OutHit.ImpactNormal.ToString()));
		}
	}
}

void AEnemy_CPP::CollisionLeft(UStaticMeshComponent* me)
{
	FVector actor_pos = GetActorLocation();
	FVector actor_vec_r = GetActorRightVector() * -100.0f;
	FHitResult OutHit;
	FCollisionQueryParams CollisionParams;
	CollisionParams.bTraceComplex = false;
	CollisionParams.AddIgnoredActor(this);

	//DrawDebugLine(GetWorld(), actor_pos, actor_pos + actor_vec_r, FColor::Blue, false);
	
	if (GetWorld()->LineTraceSingleByChannel(OutHit, actor_pos, actor_pos + actor_vec_r, ECC_Visibility, CollisionParams))
	{
		AActor* OtherActor  = OutHit.GetActor(); 
		if (OtherActor->ActorHasTag(FName(TEXT("Wall"))) == true)
		{
			//GLog->Log("Sheesh! A wall!");
			move_right = true;
		}

		if (OutHit.bBlockingHit)
		{
			//GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red,FString::Printf(TEXT("You are hitting: %s"), *OutHit.GetActor()->GetName()));
			//GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, FString::Printf(TEXT("Impact Point: %s"), *OutHit.ImpactPoint.ToString()));
			//GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, FString::Printf(TEXT("Normal Point: %s"), *OutHit.ImpactNormal.ToString()));
		}
	}
}


// Called when the game starts or when spawned
void AEnemy_CPP::BeginPlay()
{
	Super::BeginPlay();

	TArray<UStaticMeshComponent*> Components;
	this->GetComponents<UStaticMeshComponent>(Components);
	Me = Components[1];

	GLog->Log("BeginPlay");
}

// Called every frame
void AEnemy_CPP::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	if (Me)
	{
		if (move_right == true)
		{
			move_cpp = 500000.0f;
		}
		else
		{
			move_cpp = -500000.0f;
		}

		CollisionRight(Me);
		CollisionLeft(Me);
		
		GLog->Log("Meeeeeeee!");
		FVector m = GetActorRightVector() * move_cpp;
		Me->AddImpulse(m, NAME_None, false);
		SetActorLocation(Me->GetComponentLocation());
	}
}
