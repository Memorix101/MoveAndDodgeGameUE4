// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Runtime/UMG/Public/UMG.h"
#include "Blueprint/UserWidget.h"
#include "Components/InputComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Player_CPP.generated.h"

UCLASS()
class MOVEANDDODGEGAME_API APlayer_CPP : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	APlayer_CPP();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
	void Move_XAxis(float AxisValue);
	void Move_YAxis(float AxisValue);
	void SetScore(int v);
	void UpdateScore();

	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;

	UPROPERTY(EditAnywhere, Category = CPP)
	TSubclassOf<AActor> ExplosionBP;
	
private:
	//UMG - UI https://wiki.unrealengine.com/UMG,_Referencing_UMG_Widgets_in_Code#3._Create_UMG_Widget:
	UPROPERTY(EditAnywhere, Category = CPP)
	TSubclassOf<class UUserWidget> wHUDWidget;
	UUserWidget* HUDWidget;

	UPROPERTY(EditAnywhere, Category = CPP)
	TSubclassOf<class UUserWidget> wGameOver;
	UUserWidget* GameOverWidget;

	UStaticMeshComponent* Me = nullptr;
	FStrProperty* ScoreField = nullptr;
	
	int score = 0;
	bool alive = false;
	float timeout = 0.0f;
};
