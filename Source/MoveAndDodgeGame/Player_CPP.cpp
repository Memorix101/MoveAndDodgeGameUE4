// Fill out your copyright notice in the Description page of Project Settings.


#include "Player_CPP.h"

#include "CoinActor_CPP.h"

// Sets default values
APlayer_CPP::APlayer_CPP()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	AutoPossessPlayer = EAutoReceiveInput::Player0;
}

void APlayer_CPP::SetScore(int v)
{
	score += v;
}

void APlayer_CPP::UpdateScore()
{
	if(ScoreField != NULL)
	{
		ScoreField->SetPropertyValue_InContainer(HUDWidget, FString::FromInt(score));
	}
}

// Called when the game starts or when spawned
void APlayer_CPP::BeginPlay()
{
	Super::BeginPlay();
	
	TArray<UStaticMeshComponent*> Components;
	this->GetComponents<UStaticMeshComponent>(Components);
	Me = Components[0];

	alive = true;
	
	//UMG - UI
	if (wHUDWidget) // Check if the Asset is assigned in the blueprint.
	{
		// Create the widget and store it.
		HUDWidget = CreateWidget<UUserWidget>(GetWorld(), wHUDWidget);

		// now you can use the widget directly since you have a referance for it.
		// Extra check to  make sure the pointer holds the widget.
		if (wHUDWidget)
		{
			//let add it to the view port
			HUDWidget->AddToViewport();

			ScoreField = FindFProperty<FStrProperty>(HUDWidget->GetClass(), "ScoreText");
			if(ScoreField != NULL)
			{
				if(ScoreField != NULL)
				{
					UpdateScore();
				}
			}
		}
	}
}

// Called every frame
void APlayer_CPP::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	TArray<AActor*> FoundActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ACoinActor_CPP::StaticClass(), FoundActors);

	if(FoundActors.Num() == 0)
	{
		if (wGameOver) {
			GameOverWidget = CreateWidget<UUserWidget>(GetWorld(), wGameOver);
			if (wGameOver)
			{
				GameOverWidget->AddToViewport();
			}
		}
	}

	if(alive == false)
	{
		timeout += 1.0f * DeltaTime;
		if(timeout >= 2.0f)
		{
			Me->SetPhysicsLinearVelocity(FVector::ZeroVector, false, NAME_None);
			SetActorLocation(Me->GetComponentLocation());
			Me->SetSimulatePhysics(true);
			SetActorEnableCollision(true);
			for (TActorIterator<AActor> actor(GetWorld()); actor; ++actor)
			{
				if(actor->ActorHasTag(FName(TEXT("PlayerStart"))) == true)
				{
					Me->SetWorldLocation(actor->GetActorLocation());
					SetActorLocation(Me->GetComponentLocation());
					GLog->Log("Huzzah!");
				}
			}
			SetActorHiddenInGame(false);
			alive = true;
			timeout = 0.0f;
		}
	}
}

// Called to bind functionality to input
void APlayer_CPP::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);
	InputComponent->BindAxis("X_Axis", this, &APlayer_CPP::Move_XAxis);
	InputComponent->BindAxis("Y_Axis", this, &APlayer_CPP::Move_YAxis);
	//GLog->Log("SetupPlayerInputComponent");
}

void APlayer_CPP::Move_XAxis(float AxisValue)
{
	if(Me->IsSimulatingPhysics())
	{
		Me->AddImpulse(FVector(0.0f, 1000.0f, 0.0f) * AxisValue, NAME_None, false);
		SetActorLocation(Me->GetComponentLocation());
	}
	//GLog->Log("HandleHorizontalAxisInputEvent");
}

void APlayer_CPP::Move_YAxis(float AxisValue)
{
	if(Me->IsSimulatingPhysics())
	{
		Me->AddImpulse(FVector(1000.0f, 0.0f, 0.0f) * AxisValue, NAME_None, false);
		SetActorLocation(Me->GetComponentLocation());
	}
	//GLog->Log("HandleVerticalAxisInputEvent");
}

void APlayer_CPP::NotifyActorBeginOverlap(AActor* OtherActor)
{
	Super::NotifyActorBeginOverlap(OtherActor);
	if(OtherActor->ActorHasTag(FName(TEXT("Enemy"))) == true)
	{
		GLog->Log("Aaah Enemy!");
		alive = false;
		SetActorHiddenInGame(true);
		GetWorld()->SpawnActor<AActor>(ExplosionBP, Me->GetComponentLocation(), GetActorRotation());
		SetActorEnableCollision(false);
		Me->SetSimulatePhysics(false);
	}
}

