// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Enemy_CPP.generated.h"

UCLASS()
class MOVEANDDODGEGAME_API AEnemy_CPP : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AEnemy_CPP();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, Category = CPP)
	UStaticMeshComponent* RootComponentMesh;
	
private:
	bool move_right = true;
	float move_cpp = 500000.0f;
	UPROPERTY(EditAnywhere, Category = CPP)
	UStaticMeshComponent* Me;
	
	void CollisionRight(UStaticMeshComponent* me);
	void CollisionLeft(UStaticMeshComponent* me);

};
